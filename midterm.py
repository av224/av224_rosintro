#!/usr/bin/env python

#these are dependences we must add in order to run our file. provided in the tutorial.
import sys
import copy
import rospy
import moveit_commander
import moveit_msgs.msg
from math import pi

class MoveGroupUR5eInterfaceTutorial(object):
    def __init__(self):
        super(MoveGroupUR5eInterfaceTutorial, self).__init__()

        ## First initialize `moveit_commander`_ and a `rospy`_ node:
        moveit_commander.roscpp_initialize(sys.argv)
        rospy.init_node("move_group_ur5e_interface_tutorial", anonymous=True)

        ## Instantiate a `RobotCommander`_ object. Provides information such as the robot's
        ## kinematic model and the robot's current joint states
        self.robot = moveit_commander.RobotCommander()
        

        ## Instantiate a `PlanningSceneInterface`_ object.  This provides a remote interface
        ## for getting, setting, and updating the robot's internal understanding of the
        ## surrounding world:
        self.scene = moveit_commander.PlanningSceneInterface()

        ## Instantiate a `MoveGroupCommander`_ object.  This object is an interface
        ## to a planning group (group of joints).  In this tutorial the group is the primary
        ## arm joints in the Panda robot, so we set the group's name to "panda_arm".
        ## If you are using a different robot, change this value to the name of your robot
        ## arm planning group. Since using UR5E, we change to UR5E_GROUP_NAME
        ## This interface can be used to plan and execute motions:
        group_name = "manipulator"
        self.move_group = moveit_commander.MoveGroupCommander("manipulator")

        ## Create a `DisplayTrajectory`_ ROS publisher which is used to display
        ## trajectories in Rviz:
        self.display_trajectory_publisher = rospy.Publisher(
            "/move_group/display_planned_path",
            moveit_msgs.msg.DisplayTrajectory,
            queue_size=20)

        # We can get the name of the reference frame for this robot:
        self.planning_frame = self.move_group.get_planning_frame()
        print("============ Planning frame: %s" % self.planning_frame)

        # We can also print the name of the end-effector link for this group:
        self.eef_link = self.move_group.get_end_effector_link()
        print("============ End effector link: %s" % self.eef_link)

        # We can get a list of all the groups in the robot:
        self.group_names = self.robot.get_group_names()
        print("============ Available Planning Groups:", self.robot.get_group_names())

        # Sometimes for debugging it is useful to print the entire state of the
        # robot:
        print("============ Printing robot state")
        print(self.robot.get_current_state())
        print("")
        

    def go_to_joint_state(self):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        move_group = self.move_group

        ## Planning to a Joint Goal
        # We get the joint values from the group and change some of the values:
        joint_goal = move_group.get_current_joint_values()
        joint_goal[0] = 0
        joint_goal[1] = -pi/4
        joint_goal[2] = 0
        joint_goal[3] = -pi/2
        joint_goal[4] = 0
        joint_goal[5] = pi/3

        # to command the robot to move to the specified joint configuration. 
        # The wait=True argument means that the function will block until the movement is completed.
        move_group.go(joint_goal, wait=True)

        #After the robot reaches the desired joint configuration, 
        #it calls move_group.stop() to stop any residual movement.
        move_group.stop()

    def go_to_pose_goal(self):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        move_group = self.move_group


        # We can plan a motion for this group to a desired pose for the EE
        pose_goal = move_group.get_current_pose().pose
        #pose_goal.orientation.w = 1.0
        pose_goal.position.x = 0.5
        pose_goal.position.y = 0.0
        pose_goal.position.z = 0.5

        move_group.set_pose_target(pose_goal)

        ## Now, we call the planner to compute the plan and execute it.
        # `go()` returns a boolean indicating whether the planning and execution was successful.
        move_group.go(wait=True)

        # Calling `stop()` ensures that there is no residual movement
        move_group.stop()

        # It is always good to clear your targets after planning with poses.
        move_group.clear_pose_targets()

    #using this as an example to draw my initials
    """"
    def plan_cartesian_path(self, scale=1): 
        move_group = self.move_group

        ## You can plan a Cartesian path directly by specifying a list of waypoints
        ## for the end-effector to go through. If executing  interactively in a
        ## Python shell, set scale = 1.0.
        waypoints = []
        current_pose = move_group.get_current_pose().pose

        # Define waypoints for the letter "A"
        wpose = copy.deepcopy(current_pose)
        wpose.position.x += 0.1 * scale
        wpose.position.y += 0.2 * scale
        waypoints.append(copy.deepcopy(wpose))
        
        wpose.position.x += 0.1 * scale
        wpose.position.y -= 0.2 * scale
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.x += 0.05 * scale
        wpose.position.y += 0.1 * scale
        waypoints.append(copy.deepcopy(wpose))
        
        wpose.position.x += 0.1 * scale
        wpose.position.y -= 0.1 * scale
        waypoints.append(copy.deepcopy(wpose))

        wpose.position.x -= 0.1 * scale
        wpose.position.y += 0.2 * scale
        waypoints.append(copy.deepcopy(wpose))

        (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
        
        return move_group.execute(plan, wait=True)
    """

    def display_trajectory(self, plan):
        # Copy class variables to local variables to make the web tutorials more clear.
        # In practice, you should use the class variables directly unless you have a good
        # reason not to.
        robot = self.robot
        display_trajectory_publisher = self.display_trajectory_publisher

        ## Displaying a Trajectory
        ## ^^^^^^^^^^^^^^^^^^^^^^^
        ## You can ask RViz to visualize a plan (aka trajectory) for you. But the
        ## group.plan() method does this automatically so this is not that useful
        ## here (it just displays the same trajectory again):
        ##
        ## A `DisplayTrajectory`_ msg has two primary fields, trajectory_start and trajectory.
        ## We populate the trajectory_start with our current robot state to copy over
        ## any AttachedCollisionObjects and add our plan to the trajectory.
        display_trajectory = moveit_msgs.msg.DisplayTrajectory()
        display_trajectory.trajectory_start = robot.get_current_state()
        display_trajectory.trajectory.append(plan)
        # Publish
        display_trajectory_publisher.publish(display_trajectory)

    def execute_plan(self, plan):
        # Fetch the move_group from the class variable. This is primarily for readability,
        # especially for tutorial purposes.
        move_group = self.move_group

        # Execute the passed plan. The robot will follow the trajectory defined in the plan.
        move_group.execute(plan, wait=True)

        # Note: For successful execution, the robot's current joint state should be 
        # close to the initial state defined in the trajectory (RobotTrajectory).

def draw_letter_a(self, scale=1):
    """Compute a motion plan for drawing the letter 'A'."""

    print("Entering draw a")
    move_group = self.move_group  # Fetch the move_group for motion planning.
    waypoints = []  # List to store the waypoints for the trajectory.
    current_pose = move_group.get_current_pose().pose  # Get robot's current pose.

    # Define waypoints to draw the letter 'A'.

    # Start position (not explicitly added to waypoints since it's the current position).
    wpose = copy.deepcopy(current_pose)

    # Upper left tip of 'A'.
    wpose.position.x += 0.1 * scale
    wpose.position.y += 0.2 * scale
    waypoints.append(copy.deepcopy(wpose))
    
    # Bottom right tip of 'A'.
    wpose.position.x += 0.1 * scale
    wpose.position.y -= 0.2 * scale
    waypoints.append(copy.deepcopy(wpose))

    # Middle point of 'A' (towards the left).
    wpose.position.x -= 0.05 * scale
    wpose.position.y += 0.1 * scale
    waypoints.append(copy.deepcopy(wpose))

    # Bottom left starting point of 'A'.
    wpose.position.x -= 0.1 * scale
    waypoints.append(copy.deepcopy(wpose))

    # Return to starting point.
    wpose.position.x -= 0.05 * scale
    wpose.position.y -= 0.1 * scale
    waypoints.append(copy.deepcopy(wpose))

    

    # Compute a cartesian path for the robot to follow the defined waypoints.
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    return plan, fraction  # Return the computed plan and the fraction of the path that was successfully planned.
    # the fraction was used in logger to debug if certain parts of the code weren't running

    print("Exiting draw a")

def draw_letter_v(self, scale=1):
    """Compute a motion plan for drawing the letter 'V'."""

    print("Entering draw v")

    move_group = self.move_group  # Fetch the move_group for motion planning.
    waypoints = []  # List to store the waypoints for the trajectory.
    current_pose = move_group.get_current_pose().pose  # Get robot's current pose.

    # Define waypoints to draw the letter 'V'.

    # Start position (not explicitly added to waypoints since it's the current position).
    wpose = copy.deepcopy(current_pose)

    # Bottom left tip of 'V'.
    wpose.position.x += 0.1 * scale
    wpose.position.y -= 0.2 * scale
    waypoints.append(copy.deepcopy(wpose))

    # Upper right tip of 'V' (completing the 'V' shape).
    wpose.position.x += 0.1 * scale
    wpose.position.y += 0.2 * scale
    waypoints.append(copy.deepcopy(wpose))

    # Compute a cartesian path for the robot to follow the defined waypoints.
    (plan, fraction) = move_group.compute_cartesian_path(waypoints, 0.01, 0.0)
    return plan, fraction  # Return the computed plan and the fraction of the path that was successfully planned.

    print("Exiting draw v")

def main():
    try:
        # Print introductory lines for the UR5e Python Interface
        print("----------------------------------------------------------")
        print("Welcome to the UR5e Python Interface.")
        print("----------------------------------------------------------")
        print("Press Ctrl-D to exit at any time")

        # Prompt the user to initialize the moveit_commander
        # The moveit_commander is a core part of the MoveIt! library, allowing interaction with the robot
        input("============ Press `Enter` to begin the tutorial by setting up the moveit_commander ...")
        # Create an instance of the MoveGroupUR5eInterfaceTutorial class
        # This class likely contains methods to interact with a UR5e robotic arm via MoveIt!
        tutorial = MoveGroupUR5eInterfaceTutorial()

        # Prompt the user and move the robot using joint state goals
        # Joint state goals specify desired positions for each of the robot's joints
        input("============ Press `Enter` to execute a movement using a joint state goal ...")
        tutorial.go_to_joint_state()

        # Prompt the user and move the robot to a specific pose
        # Pose goals specify a desired end-effector position and orientation
        input("============ Press `Enter` to execute a movement using a pose goal ...")
        tutorial.go_to_pose_goal()

        # Prompt the user and compute a motion plan for drawing the letter 'A' with the robot
        input("============ Press `Enter` to draw the letter 'A' ...")
        plan, fraction = tutorial.draw_letter_a()

        # Execute the computed plan to draw the letter 'A'
        tutorial.move_group.execute(plan, wait=True)

        # Pause for 2 seconds to allow the motion to complete and possibly to visually confirm the action
        rospy.sleep(2.0)

        # Prompt the user and compute a motion plan for drawing the letter 'V' with the robot
        input("============ Press `Enter` to draw the letter 'V' ...")
        plan, fraction = tutorial.draw_letter_v()

        # Execute the computed plan to draw the letter 'V'
        tutorial.move_group.execute(plan, wait=True)
        
    except rospy.ROSInterruptException:
        # Handle exceptions caused by ROS interruptions (e.g., a ROS shutdown request)
        return
    except KeyboardInterrupt:
        # Handle cases where the user manually interrupts the program (e.g., by pressing Ctrl+C)
        return



if __name__ == "__main__":
    main()
