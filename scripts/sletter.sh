#!/usr/bin/bash

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[0.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

# Draw the first half of the "S" by moving forward and turning right
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 2.5]'


# Draw the middle segment of the "S" by moving forward without turning
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[1.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'


# Draw the second half of the "S" by moving forward and turning left
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
	-- '[2.0, 0.0, 0.0]' '[0.0, 0.0, -2.5]'

