
#!/usr/bin/bash

# a letter
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[0.0,1.5,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[0.0,0.0,0.0]' '[0.0,0.0,3.14159]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[0.0,.5,0.0]' '[0.0,0.0,0.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[0.0, 0.0, 0.0]' '[0.0, 0.0,1.5708]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[5.0 , 0.0 , 0.0]' '[0.0 , 0.0 , -6.5]'


# s letter
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[0.5, 0.0, 0.0]' '[0.0, 0.0, 0.0]'

# Draw the first half of the "S" by moving forward and turning right
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 2.5]'


# Draw the middle segment of the "S" by moving forward without turning
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[1.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'


# Draw the second half of the "S" by moving forward and turning left
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[2.0, 0.0, 0.0]' '[0.0, 0.0, -2.5]'

