#!/usr/bin/bash

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[0.0,1.5,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[0.0,0.0,0.0]' '[0.0,0.0,3.14159]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[0.0,.5,0.0]' '[0.0,0.0,0.0]'

rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[0.0, 0.0, 0.0]' '[0.0, 0.0,1.5708]'
rostopic pub -1 /turtle1/cmd_vel geometry_msgs/Twist \
        -- '[5.0 , 0.0 , 0.0]' '[0.0 , 0.0 , -6.5]'

