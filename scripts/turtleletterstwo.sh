#!/usr/bin/bash
rosservice call /kill "turtle1"

rosservice call /spawn "{x: 5.544445, y: 5.544445, theta: 0.0, name: 'turtleA'}"
# [Insert your rostopic pub commands for drawing 'A' here, but target /turtleA/cmd_vel instead of /turtle1/cmd_vel]

# Change 'A' letter color to red
rosservice call /turtleA/set_pen 0 0 0 1 1
rosservice call /turtleA/set_pen 255 0 0 2 0

# a letter
rostopic pub -1 /turtleA/cmd_vel geometry_msgs/Twist \
        -- '[0.0,1.5,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtleA/cmd_vel geometry_msgs/Twist \
        -- '[0.0,0.0,0.0]' '[0.0,0.0,3.14159]'
rostopic pub -1 /turtleA/cmd_vel geometry_msgs/Twist \
        -- '[0.0,.5,0.0]' '[0.0,0.0,0.0]'
rostopic pub -1 /turtleA/cmd_vel geometry_msgs/Twist \
        -- '[0.0, 0.0, 0.0]' '[0.0, 0.0,1.5708]'
rostopic pub -1 /turtleA/cmd_vel geometry_msgs/Twist \
        -- '[5.0 , 0.0 , 0.0]' '[0.0 , 0.0 , -6.5]'
rosservice call /kill "turtleA"

rosservice call /spawn "{x: 8.0, y: 4.0, theta: 0.0, name: 'turtleS'}"
# [Insert your rostopic pub commands for drawing 'S' here, but target /turtleS/cmd_vel instead of /turtle1/cmd_vel]

# Change 'S' letter color to blue
rosservice call /turtleS/set_pen 0 0 0 1 1
rosservice call /turtleS/set_pen 0 0 255 2 0

#s letter
rostopic pub -1 /turtleS/cmd_vel geometry_msgs/Twist \
        -- '[0.5, 0.0, 0.0]' 'f0.0, 0.0, 0.0]'

# Draw the first half of the "S" by moving forward and turning right
rostopic pub -1 /turtleS/cmd_vel geometry_msgs/Twist \
	-- '[2.0, 0.0, 0.0]' '[0.0, 0.0, 2.5]'


# Draw the middle segment of the "S" by moving forward without turning
rostopic pub -1 /turtleS/cmd_vel geometry_msgs/Twist \
	-- '[1.0, 0.0, 0.0]' '[0.0, 0.0, 0.0]'


# Draw the second half of the "S" by moving forward and turning left
rostopic pub -1 /turtleS/cmd_vel geometry_msgs/Twist \
	-- '[2.0, 0.0, 0.0]' '[0.0, 0.0, -2.5]'
