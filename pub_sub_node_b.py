#!/usr/bin/env python3

#  Python client library for ROS
import rospy
#  provides the String message type
from std_msgs.msg import String


# This function contains the code for initializing the node, creating a publisher, and sending messages in a loop.

def publisher_node_a():
    
    # This command initializes the node with the name 'publisher_node_a'. Each node in a ROS system must have a unique name.
    rospy.init_node('publisher_node_a')
    
    # This line creates a publisher object that publishes messages of the String type on a topic called 
    
    # 'topic_a'. The queue_size parameter is used to limit the number of messages stored in the queue if 
    
    # the subscriber cannot process the messages quickly enough.
    pub = rospy.Publisher('topic_a', String, queue_size=10)
    # publishing rate
    rate = rospy.Rate(1)  # 1 Hz

    # This loop will continue until the node is shut down. It creates a message, logs it, 
    # publishes the message using the pub.publish() method, 
    # and then sleeps for the appropriate time to achieve the desired publishing rate.
    while not rospy.is_shutdown():
        message = "Hello from Node A!"
        rospy.loginfo("Node A: Sending: %s", message)
        pub.publish(message)
        rate.sleep()

if __name__ == '__main__':
    try:
        publisher_node_a()
    except rospy.ROSInterruptException:
        pass