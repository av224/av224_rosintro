#!/usr/bin/env python3
import rospy
from std_msgs.msg import String

# This function is executed when a message is received on the subscribed topic. 
# The received message is passed as an argument called data.

def callback(data):
    # Log the received message and publish a new message
    rospy.loginfo("Node B: Received: %s", data.data)
    message = "Hello from Node B!"
    rospy.loginfo("Node B: Sending: %s", message)
    pub.publish(message)


def pub_sub_node_b():
    global pub
    
    # This function contains the code for initializing the node, 
    # creating a publisher, subscribing to a topic, and processing received messages
    rospy.init_node('pub_sub_node_b')

    # Create a publisher object and a subscriber object
    pub = rospy.Publisher('topic_b', String, queue_size=10)
    rospy.Subscriber('topic_a', String, callback)

    # Start the node's message processing loop
    rospy.spin()

if __name__ == '__main__':
    pub_sub_node_b()