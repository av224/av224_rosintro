#!/usr/bin/env python3
import rospy
from std_msgs.msg import String

def callback(data):
    rospy.loginfo("Node C: Received: %s", data.data)

def subscriber_node_c():
    rospy.init_node('subscriber_node_c')
    rospy.Subscriber('topic_b', String, callback)
    rospy.spin()

if __name__ == '__main__':
    subscriber_node_c()